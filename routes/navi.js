/*
 * GET navi-params.
 */

exports.getRouteJson = function(req, res){
    var id = req.params.routeID;

    var pluginArrayArg = [];

    var point1 = {}; point1.day=3; point1.hour=18; point1.lat=6.1; point1.lon=153.3; point1.class= 2; pluginArrayArg.push(point1);
    var point2 = {}; point2.day=3; point2.hour=21; point2.lat=6.1; point2.lon=152.8; point2.class= 2; pluginArrayArg.push(point2);
    var point3 = {}; point3.day=4; point3.hour=0; point3.lat=6.1; point3.lon=152.2; point3.class= 3; pluginArrayArg.push(point3);
    var point4 = {}; point4.day=4; point4.hour=3; point4.lat=6.2; point4.lon=151.2; point4.class= 3; pluginArrayArg.push(point4);
    var point5 = {}; point5.day=4; point5.hour=6; point5.lat=6.2; point5.lon=150.4; point5.class= 3; pluginArrayArg.push(point5);
    var point6 = {}; point6.day=4; point6.hour=9; point6.lat=6.2; point6.lon=149.5; point6.class= 3; pluginArrayArg.push(point6);
    var point7 = {}; point7.day=4; point7.hour=12; point7.lat=6.3; point7.lon=148.6; point7.class= 3; pluginArrayArg.push(point7);
    var point8 = {}; point8.day=4; point8.hour=15; point8.lat=6.3; point8.lon=148.4; point8.class= 3; pluginArrayArg.push(point8);
    var point9 = {}; point9.day=4; point9.hour=18; point9.lat=6.5; point9.lon=147.6; point9.class= 3; pluginArrayArg.push(point9);
    var point10 = {}; point10.day=4; point10.hour=21; point10.lat=6.5; point10.lon=147.0; point10.class= 3; pluginArrayArg.push(point10);
    var point11 = {}; point11.day=5; point11.hour=0; point11.lat=6.5; point11.lon=145.9; point11.class= 4; pluginArrayArg.push(point11);
    var point12 = {}; point12.day=5; point12.hour=3; point12.lat=6.5; point12.lon=145.2; point12.class= 4; pluginArrayArg.push(point12);
    var point13 = {}; point13.day=5; point13.hour=6; point13.lat=6.5; point13.lon=144.6; point13.class= 4; pluginArrayArg.push(point13);
    var point14 = {}; point14.day=5; point14.hour=9; point14.lat=6.5; point14.lon=144.0; point14.class= 4; pluginArrayArg.push(point14);
    var point15 = {}; point15.day=5; point15.hour=12; point15.lat=6.9; point15.lon=143.1; point15.class= 4; pluginArrayArg.push(point15);
    var point16 = {}; point16.day=5; point16.hour=15; point16.lat=7.0; point16.lon=142.1; point16.class= 4; pluginArrayArg.push(point16);
    var point17 = {}; point17.day=5; point17.hour=18; point17.lat=7.1; point17.lon=141.3; point17.class= 5; pluginArrayArg.push(point17);
    var point18 = {}; point18.day=5; point18.hour=21; point18.lat=7.3; point18.lon=140.5; point18.class= 5; pluginArrayArg.push(point18);
    var point19 = {}; point19.day=6; point19.hour=0; point19.lat=7.3; point19.lon=139.7; point19.class= 5; pluginArrayArg.push(point19);
    var point20 = {}; point20.day=6; point20.hour=3; point20.lat=7.5; point20.lon=138.9; point20.class= 5; pluginArrayArg.push(point20);
    var point21 = {}; point21.day=6; point21.hour=6; point21.lat=7.6; point21.lon=138.0; point21.class= 5; pluginArrayArg.push(point21);
    var point22 = {}; point22.day=6; point22.hour=9; point22.lat=7.7; point22.lon=137.2; point22.class= 5; pluginArrayArg.push(point22);
    var point23 = {}; point23.day=6; point23.hour=12; point23.lat=7.9; point23.lon=136.2; point23.class= 5; pluginArrayArg.push(point23);
    var point24 = {}; point24.day=6; point24.hour=15; point24.lat=8.1; point24.lon=135.3; point24.class= 5; pluginArrayArg.push(point24);
    var point25 = {}; point25.day=6; point25.hour=18; point25.lat=8.2; point25.lon=134.4; point25.class= 5; pluginArrayArg.push(point25);
    var point26 = {}; point26.day=6; point26.hour=21; point26.lat=8.4; point26.lon=133.6; point26.class= 5; pluginArrayArg.push(point26);
    var point27 = {}; point27.day=7; point27.hour=0; point27.lat=8.7; point27.lon=132.8; point27.class= 5; pluginArrayArg.push(point27);
    var point28 = {}; point28.day=7; point28.hour=3; point28.lat=9.0; point28.lon=131.9; point28.class= 5; pluginArrayArg.push(point28);
    var point29 = {}; point29.day=7; point29.hour=6; point29.lat=9.3; point29.lon=131.1; point29.class= 5; pluginArrayArg.push(point29);
    var point30 = {}; point30.day=7; point30.hour=9; point30.lat=9.8; point30.lon=130.2; point30.class= 5; pluginArrayArg.push(point30);
    var point31 = {}; point31.day=7; point31.hour=12; point31.lat=10.2; point31.lon=129.1; point31.class= 5; pluginArrayArg.push(point31);
    var point32 = {}; point32.day=7; point32.hour=15; point32.lat=10.4; point32.lon=128.0; point32.class= 5; pluginArrayArg.push(point32);
    var point33 = {}; point33.day=7; point33.hour=18; point33.lat=10.6; point33.lon=126.9; point33.class= 5; pluginArrayArg.push(point33);
    var point34 = {}; point34.day=7; point34.hour=21; point34.lat=10.8; point34.lon=125.9; point34.class= 5; pluginArrayArg.push(point34);
    var point35 = {}; point35.day=8; point35.hour=0; point35.lat=11.0; point35.lon=124.8; point35.class= 5; pluginArrayArg.push(point35);
    var point36 = {}; point36.day=8; point36.hour=3; point36.lat=11.2; point36.lon=123.7; point36.class= 5; pluginArrayArg.push(point36);
    var point37 = {}; point37.day=8; point37.hour=6; point37.lat=11.4; point37.lon=122.6; point37.class= 5; pluginArrayArg.push(point37);
    var point38 = {}; point38.day=8; point38.hour=9; point38.lat=11.5; point38.lon=121.6; point38.class= 5; pluginArrayArg.push(point38);
    var point39 = {}; point39.day=8; point39.hour=12; point39.lat=11.8; point39.lon=120.7; point39.class= 5; pluginArrayArg.push(point39);
    var point40 = {}; point40.day=8; point40.hour=15; point40.lat=12.3; point40.lon=119.4; point40.class= 5; pluginArrayArg.push(point40);
    var point41 = {}; point41.day=8; point41.hour=18; point41.lat=12.4; point41.lon=118.2; point41.class= 5; pluginArrayArg.push(point41);
    var point42 = {}; point42.day=8; point42.hour=21; point42.lat=12.5; point42.lon=117.3; point42.class= 5; pluginArrayArg.push(point42);
    var point43 = {}; point43.day=9; point43.hour=0; point43.lat=12.3; point43.lon=116.6; point43.class= 5; pluginArrayArg.push(point43);
    var point44 = {}; point44.day=9; point44.hour=3; point44.lat=12.9; point44.lon=115.6; point44.class= 5; pluginArrayArg.push(point44);
    var point45 = {}; point45.day=9; point45.hour=9; point45.lat=13.9; point45.lon=113.9; point45.class= 5; pluginArrayArg.push(point45);
    var point46 = {}; point46.day=9; point46.hour=12; point46.lat=14.4; point46.lon=113.1; point46.class= 5; pluginArrayArg.push(point46);
    var point47 = {}; point47.day=9; point47.hour=15; point47.lat=15.0; point47.lon=112.2; point47.class= 5; pluginArrayArg.push(point47);
    var point48 = {}; point48.day=9; point48.hour=18; point48.lat=15.4; point48.lon=111.4; point48.class= 5; pluginArrayArg.push(point48);
    var point49 = {}; point49.day=9; point49.hour=21; point49.lat=15.9; point49.lon=111.1; point49.class= 5; pluginArrayArg.push(point49);
    var point50 = {}; point50.day=10; point50.hour=0; point50.lat=16.5; point50.lon=110.3; point50.class= 5; pluginArrayArg.push(point50);
    var point51 = {}; point51.day=10; point51.hour=3; point51.lat=17.0; point51.lon=109.7; point51.class= 5; pluginArrayArg.push(point51);
    var point52 = {}; point52.day=10; point52.hour=6; point52.lat=17.8; point52.lon=109.0; point52.class= 5; pluginArrayArg.push(point52);
    var point53 = {}; point53.day=10; point53.hour=9; point53.lat=18.5; point53.lon=108.4; point53.class= 5; pluginArrayArg.push(point53);
    var point54 = {}; point54.day=10; point54.hour=12; point54.lat=19.4; point54.lon=108.1; point54.class= 5; pluginArrayArg.push(point54);
    var point55 = {}; point55.day=10; point55.hour=15; point55.lat=19.8; point55.lon=107.9; point55.class= 5; pluginArrayArg.push(point55);
    var point56 = {}; point56.day=10; point56.hour=18; point56.lat=20.3; point56.lon=107.5; point56.class= 4; pluginArrayArg.push(point56);
    var point57 = {}; point57.day=10; point57.hour=21; point57.lat=20.8; point57.lon=107.1; point57.class= 4; pluginArrayArg.push(point57);
    var point58 = {}; point58.day=11; point58.hour=0; point58.lat=21.3; point58.lon=107.2; point58.class= 4; pluginArrayArg.push(point58);
    var point59 = {}; point59.day=11; point59.hour=3; point59.lat=22.0; point59.lon=107.2; point59.class= 4; pluginArrayArg.push(point59);
    var point60 = {}; point60.day=11; point60.hour=6; point60.lat=22.3; point60.lon=107.4; point60.class= 3; pluginArrayArg.push(point60);
    var point61 = {}; point61.day=11; point61.hour=9; point61.lat=22.6; point61.lon=107.6; point61.class= 3; pluginArrayArg.push(point61);
    var point62 = {}; point62.day=11; point62.hour=12; point62.lat=23.0; point62.lon=107.0; point62.class= 2; pluginArrayArg.push(point62);

    var jsonArray = JSON.parse(JSON.stringify(pluginArrayArg));

    res.json(jsonArray);
};