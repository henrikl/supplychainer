d3.json(jsonLoc, function (error, track) {
    d3.json('/data/world-50m.json', function (error, world) {
        var color_scale = d3.scale.quantile().domain([1, 5]).range(colorbrewer.YlOrRd[5]);
        var filter = svg.append("defs")
            .append("filter")
            .attr("id", "drop-shadow")
            .attr("height", "130%");
        filter.append("feGaussianBlur")
            .attr("in", "SourceAlpha")
            .attr("stdDeviation", 5)
            .attr("result", "blur");
        filter.append("feOffset")
            .attr("in", "blur")
            .attr("dx", 5)
            .attr("dy", 5)
            .attr("result", "offsetBlur");
        var feMerge = filter.append("feMerge");
        feMerge.append("feMergeNode")
            .attr("in", "offsetBlur")
        feMerge.append("feMergeNode")
            .attr("in", "SourceGraphic");
        svg.insert("path", ".graticule")
            .datum(topojson.feature(world, world.objects.land))
            .attr("class", "land")
            .attr("d", path)
            .style("filter", "url(#drop-shadow)");
        svg.insert("path", ".graticule")
            .datum(topojson.mesh(world, world.objects.countries, function (a, b) {
                return a !== b;
            }))
            .attr("class", "boundary")
            .attr("d", path);
        var dateText = svg.append("text")
            .attr("id", "dataTitle")
            .text("2013/11/" + track[0].day + " " + track[0].hour + ":00 class: " + track[0].class)
            .attr("x", 70)
            .attr("y", 20)
            .attr("font-family", "sans-serif")
            .attr("font-size", "20px")
            .attr("fill", color_scale(track[0].class));
        var pathLine = d3.svg.line()
            .interpolate("cardinal")
            .x(function (d) {
                return projection([d.lon, d.lat])[0];
            })
            .y(function (d) {
                return projection([d.lon, d.lat])[1];
            });
        var haiyanPath = svg.append("path")
            .attr("d", pathLine(track))
            .attr("fill", "none")
            .attr("stroke", color_scale(track[0].class))
            .attr("stroke-width", 3)
            .style('stroke-dasharray', function (d) {
                var l = d3.select(this).node().getTotalLength();
                return l + 'px, ' + l + 'px';
            })
            .style('stroke-dashoffset', function (d) {
                return d3.select(this).node().getTotalLength() + 'px';
            });
        var haiyanPathEl = haiyanPath.node();
        var haiyanPathElLen = haiyanPathEl.getTotalLength();
        var pt = haiyanPathEl.getPointAtLength(0);

        var icon = svg.append("circle")
            .attr("r", 30)
            .attr("transform", "translate(" + pt.x + "," + pt.y + "), scale(" + (0.15 * track[0].class) + ")")
            .attr("fill", color_scale(track[0].class))
            .attr("class", "icon");

        var i = 0;
        var animation = setInterval(function () {
            pt = haiyanPathEl.getPointAtLength(haiyanPathElLen * i / track.length);
            icon
                .transition()
                .ease("linear")
                .duration(1000)
                .attr("transform", "translate(" + pt.x + "," + pt.y + "), scale(" + (0.15 * track[i].class) + "), rotate(" + (i * 15) + ")")
                .attr("fill", color_scale(track[i].class));
            haiyanPath
                .transition()
                .duration(1000)
                .ease("linear")
                .attr("stroke", color_scale(track[i].class))
                .style('stroke-dashoffset', function (d) {
                    var stroke_offset = (haiyanPathElLen - haiyanPathElLen * i / track.length + 9);
                    return (haiyanPathElLen < stroke_offset) ? haiyanPathElLen : stroke_offset + 'px';
                });
            dateText
                .text("2013/11/" + track[i].day + " " + track[i].hour + ":00 class: " + track[i].class)
                .attr("fill", color_scale(track[i].class));
            i = i + 1;
            if (i == track.length)
                clearInterval(animation)
        }, 1000);
    });
});
d3.select(self.frameElement).style("height", height + "px");
