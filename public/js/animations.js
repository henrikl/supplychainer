function formatDate(date) {
    hours = date.getHours();
    hours = hours < 10 ? '0' + hours : hours;
    minutes = date.getMinutes();
    minutes = minutes < 10 ? '0' + minutes : minutes;
    day = date.getDate();
    day = day < 10 ? '0' + day : day;
    strTime = hours + ':' + minutes;
    return day + '.' + (date.getMonth()+1) + "." + date.getFullYear() + "  " + strTime;
}

function createInfoScreen(svg, height) {
    var infoTextArray = [];

    // Luodaan inforuutu
    infoscreenHeight = 120;
    infoscreenWidth = 200;
    var infoScreen = svg.append("rect")
        .attr("id", "infoScreen")
        .attr("x", 0)
        .attr("y", height - infoscreenHeight)
        .attr("width", infoscreenWidth)
        .attr("height", infoscreenHeight)
        .attr("fill", 'grey')
        .attr("stroke", 'black')
        .attr("fill-opacity", 0.4);

    // Tekstirivit
    var infoText1 = svg.append("text")
        .attr("id", "infoText1")
        .text("")
        .attr("x", 10)
        .attr("y", height - infoscreenHeight + 20)
        .attr("font-family", "sans-serif")
        .attr("font-size", "10px")
        .attr("fill", 'black');
    infoTextArray.push(infoText1);

    var infoText2 = svg.append("text")
        .attr("id", "infoText2")
        .text("")
        .attr("x", 20)
        .attr("y", height - infoscreenHeight + 35)
        .attr("font-family", "sans-serif")
        .attr("font-size", "8px")
        .attr("fill", 'black');
    infoTextArray.push(infoText2);

    var infoText3 = svg.append("text")
        .attr("id", "infoText3")
        .text("")
        .attr("x", 10)
        .attr("y", height - infoscreenHeight + 55)
        .attr("font-family", "sans-serif")
        .attr("font-size", "10px")
        .attr("fill", 'black');
    infoTextArray.push(infoText3);

    var infoText4 = svg.append("text")
        .attr("id", "infoText4")
        .text("")
        .attr("x", 20)
        .attr("y", height - infoscreenHeight + 70)
        .attr("font-family", "sans-serif")
        .attr("font-size", "8px")
        .attr("fill", 'black');
    infoTextArray.push(infoText4);

    var infoText5 = svg.append("text")
        .attr("id", "infoText5")
        .text("")
        .attr("x", 10)
        .attr("y", height - infoscreenHeight + 90)
        .attr("font-family", "sans-serif")
        .attr("font-size", "10px")
        .attr("fill", 'black');
    infoTextArray.push(infoText5);

    var infoText6 = svg.append("text")
        .attr("id", "infoText6")
        .text("")
        .attr("x", 20)
        .attr("y", height - infoscreenHeight + 105)
        .attr("font-family", "sans-serif")
        .attr("font-size", "8px")
        .attr("fill", 'black');
    infoTextArray.push(infoText6);

    return infoTextArray;
}

$(function() {

    var routeports = [
        { "port": "Hamburg", "lon": 14.5366, "lat": 55.4008 },
        { "port": "Rotterdam", "lon": 0.096318, "lat": 50.5252 },
        { "port": "Singapore", "lon": 104.077, "lat": 1.24667 },
        { "port": "Gresik", "lon": 114.383, "lat": -4.459 }
    ];

    var currentWidth = $('#map').width();
    var width = 938;
    var height = 620;

    var speed = 100;
    var color_scale = d3.scale.quantile().domain([1, 5]).range(colorbrewer.YlOrRd[5]);

    var projection = d3.geo
        .mercator()
        .scale(150)
        .translate([width / 2, height / 1.41]);

    var path = d3.geo
        .path()
        .pointRadius(2)
        .projection(projection);

    // Luodaan SVG
    var svg = d3.select("#map")
        .append("svg")
        .attr("preserveAspectRatio", "xMidYMid")
        .attr("viewBox", "0 0 " + width + " " + height)
        .attr("width", currentWidth)
        .attr("height", currentWidth * height / width);

        function loaded(error, world, countries, ports) {

            var g = svg.append("g");

            // Kartan varjot ja pieni blurri.
            var filter = svg.append("defs")
                .append("filter")
                .attr("id", "drop-shadow")
                .attr("height", "130%");
            filter.append("feGaussianBlur")
                .attr("in", "SourceAlpha")
                .attr("stdDeviation", 5)
                .attr("result", "blur");
            filter.append("feOffset")
                .attr("in", "blur")
                .attr("dx", 5)
                .attr("dy", 5)
                .attr("result", "offsetBlur");
            var feMerge = filter.append("feMerge");
            feMerge.append("feMergeNode")
                .attr("in", "offsetBlur")
            feMerge.append("feMergeNode")
                .attr("in", "SourceGraphic");

            svg.insert("path", ".graticule")
                .datum(topojson.feature(world, world.objects.land))
                .attr("class", "land")
                .attr("d", path)
                .style("filter", "url(#drop-shadow)");

        svg.append("g")
            .attr("class", "countries")
            .selectAll("path")
            .data(topojson.feature(countries, countries.objects.countries).features)
            .enter()
            .append("path")
            .attr("d", path);

        svg.append("g")
            .attr("class", "ports")
            .selectAll("path")
            .data(topojson.feature(ports, ports.objects.ports).features)
            .enter()
            .append("path")
            .attr("id", function(d) {return d.id;})
            .attr("d", path);

        // Reitin satamat
        svg.selectAll("routeports")
            .data(routeports)
            .enter()
            .append("circle", "routeports")
            .attr("class", "routeport")
            .attr("r", 1)
            .attr("transform", function(d) {
            return "translate(" + projection([
                    d.lon,
                    d.lat
                ]) + ")"
            });

             // Satamien nimet
            for (j = 0; j < routeports.length; j++) {
                svg.append("text")
                    .attr("transform", "translate(" + projection([ routeports[j].lon+3, routeports[j].lat-1 ]) + ")")
                    .text(routeports[j].port)
                    .attr("font-family", "sans-serif")
                    .attr("font-size", "8px")
                    .attr("fill", 'black');
            }

            // Luodaan inforuutu
            var infoTextArr = createInfoScreen(svg, height);

            d3.json('/routewaypoints', function (error, track) {
            var dateText = svg.append("text")
                .attr("id", "dataTitle")
                .text(formatDate(new Date(track[0].date_time)))
                .attr("x", 100)
                .attr("y", height - infoscreenHeight - 5)
                .attr("font-family", "sans-serif")
                .attr("font-size", "12px")
                .attr("fill", 'black');

            var pathLine = d3.svg.line()
                .interpolate("cardinal")
                .x(function (d) {
                    return projection([d.lon, d.lat])[0];
                })
                .y(function (d) {
                    return projection([d.lon, d.lat])[1];
                });

                var shipNaviPath = svg.append("path")
                    .attr("d", pathLine(track))
                    .attr("fill", "none")
                    .attr("stroke", color_scale(3))
                    .attr("stroke-width", 3)
                    .style('stroke-dasharray', function (d) {
                        var l = d3.select(this).node().getTotalLength();
                        return l + 'px, ' + l + 'px';
                    })
                    .style('stroke-dashoffset', function (d) {
                        return d3.select(this).node().getTotalLength() + 'px';
                    });

                var haiyanPathEl = shipNaviPath.node();
                var haiyanPathElLen = haiyanPathEl.getTotalLength();
                var pt = haiyanPathEl.getPointAtLength(0);

            var icon = svg.append("circle")
                .attr("r", 10)
                .attr("transform", "translate(" + pt.x + "," + pt.y + "), scale(" + (0.15 * track[0].class) + ")")
                .attr("fill", 'red')
                .attr("class", "icon");

            // Asetetaan 1. kuljetuksen tiedot
            var textLineCount = 0;
            var latestLoading = track[0].loading;
            var latestDischarge = track[0].discharge;
            infoTextArr[textLineCount++].text("Transport: " + track[0].transport);
            infoTextArr[textLineCount++].text(latestLoading + ' - ' + latestDischarge);

            var i = 0;
            var animation = setInterval(function () {
                pt = haiyanPathEl.getPointAtLength(haiyanPathElLen * i / track.length);
                icon
                    .transition()
                    .ease("linear")
                    .duration(speed)
                    .attr("transform", "translate(" + pt.x + "," + pt.y + "), scale(" + (0.15 * track[i].class) + "), rotate(" + (i * 15) + ")")
                    .attr("fill", 'red');
                shipNaviPath
                    .transition()
                    .duration(speed)
                    .ease("linear")
                    .attr("stroke", color_scale(3))
                    .style('stroke-dashoffset', function (d) {
                        var stroke_offset = (haiyanPathElLen - haiyanPathElLen * i / track.length + 9);
                        return (haiyanPathElLen < stroke_offset) ? haiyanPathElLen : stroke_offset + 'px';
                    });
                dateText
                    .text(formatDate(new Date(track[i].date_time)))
                    .attr("fill", 'black');

                if (track[i] &&
                    latestLoading &&
                    track[i].loading &&
                    latestLoading != track[i].loading) {
                        latestLoading = track[i].loading;
                        latestDischarge = track[i].discharge;
                        infoTextArr[textLineCount++].text("Transport: " + track[i].transport);
                        infoTextArr[textLineCount++].text(latestLoading + ' - ' + latestDischarge);
                }

                if (track[i])
                i = i + 1;
                if (i == track.length)
                    clearInterval(animation)
            }, speed);
        });
    }

    queue().defer(d3.json, "/data/world-50m.json")
        .defer(d3.json, "/data/countries.topo.json")
        .defer(d3.json, "/data/ports.topo.json")
        .await(loaded);

    $(window).resize(function() {
        currentWidth = $("#map").width();
        svg.attr("width", currentWidth);
        svg.attr("height", currentWidth * height / width);
    });
});