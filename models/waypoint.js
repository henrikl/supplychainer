/**
 * Created by SEL on 10.4.2015.
 */

// grab the things we need
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var waypointSchema = new Schema({
    transport: String,
    day: Number,
    hour: Number,
    date_time: Date,
    lat: Number,
    lon: Number,
    class: Number,
    route: Number,
    loading: String,
    discharge: String
    //speed
    //distance
});

//userSchema.methods.dudify = function() {
//    // add some stuff to the users name
//    this.name = this.name + '-dude';
//
//    return this.name;
//};

// the schema is useless so far
// we need to create a model using it
var Waypoint = mongoose.model('Waypoint', waypointSchema);

// make this available to our users in our Node applications
module.exports = Waypoint;
