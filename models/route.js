/**
 * Created by SEL on 10.4.2015.
 */

/**
 * Created by SEL on 10.4.2015.
 */

// grab the things we need
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Waypoint = require('../models/waypoint');

// create a schema
//var purc1 = {}; purc1.key=1; purc1.number='54321B00008'; purc1.transport='Phoebe'; purc1.loading='Pasir Gudang'; purc1.loading_date= '24.11.2013'; pluginArrayArg.push(purc1);
var routeSchema = new Schema({
    key: Number,
    number: String,
    transport: String,
    loading_time: Date,
    loading_location: String,
    discharge_time: Date,
    discharge_location: String,
    waypoints: [Waypoint.schema],
    //waypoints: String,
    next_routes: [Number]
});

//userSchema.methods.dudify = function() {
//    // add some stuff to the users name
//    this.name = this.name + '-dude';
//
//    return this.name;
//};

// the schema is useless so far
// we need to create a model using it
var Route = mongoose.model('Route', routeSchema);

// make this available to our users in our Node applications
module.exports = Route;
